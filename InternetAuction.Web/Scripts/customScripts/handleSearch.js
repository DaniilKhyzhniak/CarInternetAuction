﻿$('body').on('submit', '#searchForm', (e) => handleSearch(e));

function handleSearch(e) {
    e.preventDefault();
    var form = $('#searchForm');
    var url = form.attr('action');
    var type = form.attr('method');

    $.ajax({
        type: type,
        url: url,
        data: form.serialize(),
        success: function (data) {
            if (typeof data == "string") {
                document.open();
                document.write(data);
                document.close();
            }
        }
    });
};