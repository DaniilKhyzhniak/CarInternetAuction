﻿using InternetAuction.DAL.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InternetAuction.DAL.Entities
{
    public class TechnicalPassport
    {
        [Key, ForeignKey("Car")]
        public int CarId { get; set; }

        [Required, RegularExpression(@"^(?=.*[0-9])(?=.*[A-z])[0-9A-z-]{17}$")]
        [StringLength(17, MinimumLength = 17, ErrorMessage = "VIN length must be 17")]
        public string VIN { get; set; }

        [Required]
        public Transmission Transmission { get; set; }

        [Required]
        public DriveUnit DriveUnit { get; set; }

        [Required]
        public BodyType BodyType { get; set; }

        [MaxLength(50)]
        public string PrimaryDamage { get; set; }

        [Required]
        public bool HasKeys { get; set; }

        public Car Car { get; set; }
    }
}
